<?php
$title = "Reservierung anzeigen";
include '../layouts/top.php';
require_once('../../models/Reservation.php');
require_once ('../../models/Guest.php');

// View single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if(empty($_GET['id'])){
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    // Load single item per ID
    $r = Reservation::get($_GET['id']);
}

// Check if item could be found
if($r == null){
    http_response_code(404);
    die();
}
$g = Guest::get($r->getMainGuestId());

?>


    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $r->getId()?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $r->getId()?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Reservierungsnummer</th>
                <td><?=$r->getId()?></td>
            </tr>
            <tr>
                <th>Ankunftsdatum</th>
                <td><?=$r->getStartDate()?></td>
            </tr>
            <tr>
                <th>Abreise</th>
                <td><?=$r->getEndDate()?></td>
            </tr>
            <tr>
                <th>ZimmerNr</th>
                <td><?=$r->getRoomId()?></td>
            </tr>
            <tr>
                <th>Gast</th>
                <td><?=$g->getName()?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>