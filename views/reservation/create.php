<?php
$title = "Reservierung erstellen";
include '../layouts/top.php';

require_once ('../../models/Reservation.php');
$r = new Reservation();
if(!empty($_POST)){
    $r->setStartDate(isset($_POST['arrivalDate']) ? $_POST['arrivalDate'] : '');
    $r->setEndDate(isset($_POST['departureDate']) ? $_POST['departureDate'] : '');
    $r->setRoomId(isset($_POST['roomId']) ? $_POST['roomId'] : '');
    $r->setMainGuestId(isset($_POST['mainGuestId']) ? $_POST['mainGuestId'] : '');
    $r->create();

    header('Location: view.php?id=' . $r->getId());
    exit();


}

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Ankunftsdatum *</label>
                        <input type="date" class="form-control" name="arrivalDate" maxlength="64" value="">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Abreisedatum *</label>
                        <input type="date" class="form-control" name="departureDate" maxlength="64" value="">
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">ZimmerNr *</label>
                        <input type="number" class="form-control" name="roomId" maxlength="64" min="1" value="">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">GastID *</label>
                        <input type="number" class="form-control" name="mainGuestId" value="">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1">

                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>