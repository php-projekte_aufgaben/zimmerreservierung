<?php
$title = "Reservierung löschen";
include '../layouts/top.php';

require_once('../../models/Reservation.php');

// Get ID fo item (HTTP GET) for showing
$id = !empty($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0;

if(!empty($_POST['id'])){

    if(!is_numeric($_POST['id'])){
        http_response_code(400);
        die();
    }

    Reservation::delete($_POST['id']);

    header('Location: index.php');
    exit();
} else {
    $r = Reservation::get($id); //Load item for showing

    // Check if item could be found
    if($r == null){
        http_response_code(404); // Item not found
        die();
    }
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?=$r->getId()?>" method="post">
            <input type="hidden" name="id" value="<?=$r->getId()?>"/>
            <p class="alert alert-error">Wollen Sie die Reservierung wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>