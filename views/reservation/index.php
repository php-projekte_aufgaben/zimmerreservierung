<?php
$title = "Reservierungsübersicht";
include '../layouts/top.php';
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ReservierungsNr</th>
                    <th>Ankunftsdatum</th>
                    <th>Abreisedatum</th>
                    <th>ZimmerNr</th>
                    <th>GastID</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Reservation.php';
                $rs= Reservation::getAll();
                foreach ($rs as $r){
                    echo '<tr>';
                    echo '<td>' . $r->getId() . '</td>';
                    echo '<td>' . $r->getStartDate() . '</td>';
                    echo '<td>' . $r->getEndDate() . '</td>';
                    echo '<td>' . $r->getRoomId() . '</td>';
                    echo '<td>' . $r->getMainGuestId() . '</td>';
                    echo '<td>';

                    echo '<a class="btn btn-info" href="view.php?id=' . $r->getId() . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-primary" href="update.php?id=' . $r->getId() . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-danger" href="delete.php?id=' . $r->getId() . '"><span class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>