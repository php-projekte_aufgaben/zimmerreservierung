<?php
$title = "Reserviuerng bearbeiten";
include '../layouts/top.php';
require_once('../../models/Reservation.php');

// View single item (per ID), redirect to index if no Id is present (HTTP GET-parameter)
if(empty($_GET['id'])){
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    // Load single item per ID
    $r = Reservation::get($_GET['id']);
}

// Check if item could be found
if($r == null){
    http_response_code(400); // Item not found
    die();
}

if(!empty($_POST)){
    $r->setStartDate(isset($_POST['arrivalDate']) ? $_POST['arrivalDate'] : '');
    $r->setEndDate(isset($_POST['departureDate']) ? $_POST['departureDate'] : '');
    $r->setRoomId(isset($_POST['roomId']) ? $_POST['roomId'] : '');
    $r->setMainGuestId(isset($_POST['mainGuestId']) ? $_POST['mainGuestId'] : '');
    // Upade existing item and redirect to index-page
    $r->update();
    header('Location: index.php');
    exit();
}

?>


<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $r->getId()?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Ankunftsdatum *</label>
                    <input type="date" class="form-control" name="arrivalDate" maxlength="64" value="<?=$r->getStartDate()?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Abreisedatum *</label>
                    <input type="date" class="form-control" name="departureDate" maxlength="64" value="<?=$r->getEndDate()?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">ZimmerNr *</label>
                    <input type="number" class="form-control" name="roomId" maxlength="64" min="1" value="<?=$r->getRoomId()?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">GastID *</label>
                    <input type="number" class="form-control" name="mainGuestId" value="<?=$r->getMainGuestId()?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">

            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
