<?php
$title = "Gast löschen";
include '../layouts/top.php';

require_once ('../../models/Guest.php');

//get ID of item (HTTP GET) for showing
$id = !empty($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0;

if(!empty($_POST['id'])){

    if(!is_numeric($_POST['id'])){
        http_response_code(400); // 400 => "Bad Request"
        die();
    }

    Guest::delete($_POST['id']);

    header('Location: index.php');
    exit();
} else {
    $g = Guest::get($id); //load item for showing

    if($g == null){
        http_response_code(404); //item not found (404 => "Not Found")
        die();
    }
}
?>


    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?=$g->getId()?>" method="post">
            <input type="hidden" name="id" value="<?=$g->getId()?>"/>
            <p class="alert alert-error">Wollen Sie den Gast: <?=$g->getName()?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>