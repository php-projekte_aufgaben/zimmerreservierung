<?php
$title = "Gast bearbeiten";
include '../layouts/top.php';
require_once('../../models/Guest.php');

// View single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])) {
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400); // 400 => "Bad Request"
    die();
} else {
    //load single item per ID
    $guest = Guest::get($_GET['id']);
}

// Check if item could be found
if ($guest == null) {
    http_response_code(404); // Item not found (404 => "Not Found")
    die();
}

if (!empty($_POST)) {
    $guest->setName(isset($_POST['name']) ? $_POST['name'] : '');
    $guest->setEmail(isset($_POST['email']) ? $_POST['email'] : '');
    $guest->setAddress(isset($_POST['address']) ? $_POST['address'] : '');

    // Update existing item and redirect to index-page
    if ($guest->save()) {
        header('Location: index.php');
        exit();
    }
}

?>


<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $guest->getId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="64" value="<?= $guest->getName() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Email *</label>
                    <input type="email" class="form-control" name="email" maxlength="64"
                           value="<?= $guest->getEmail() ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Adresse *</label>
                    <input type="text" class="form-control" name="address" min="1" value="<?= $guest->getAddress() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
