<?php
$title = "Gastverwaltung";
include '../layouts/top.php';
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>GastId</th>
                    <th>GastName</th>
                    <th>Email</th>
                    <th>Adresse</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Guest.php';
                $guests= Guest::getAll();
                foreach ($guests as $guest){
                    echo '<tr>';
                    echo '<td>' . $guest->getId() . '</td>';
                    echo '<td>' . $guest->getName() . '</td>';
                    echo '<td>' . $guest->getEmail() . '</td>';
                    echo '<td>' . $guest->getAddress() . '</td>';
                    echo '<td>';

                    echo '<a class="btn btn-info" href="view.php?id=' . $guest->getId() . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-primary" href="update.php?id=' . $guest->getId() . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-danger" href="delete.php?id=' . $guest->getId() . '"><span class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>