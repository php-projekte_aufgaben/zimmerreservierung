<?php
$title = "Gast anzeigen";
include '../layouts/top.php';
require_once('../../models/Guest.php');

// View single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])){
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    //Load single item per ID
    $guest = Guest::get($_GET['id']);
}

// Check if item could be found
if($guest == null){
    http_response_code(404);
    die();
}
?>


    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $guest->getId()?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $guest->getId()?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>GastId</th>
                <td><?=$guest->getId()?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?=$guest->getName()?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?=$guest->getEmail()?></td>
            </tr>
            <tr>
                <th>Adresse</th>
                <td><?=$guest->getAddress()?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>