<?php
$title = "Zimmerverwaltung";
include '../layouts/top.php';
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Zimmernummer</th>
                    <th>Name</th>
                    <th>Personen</th>
                    <th>Preis</th>
                    <th>Balkon</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Room.php';
                $rooms = Room::getAll();
                foreach ($rooms as $room){
                    echo '<tr>';
                    echo '<td>' . $room->getNr() . '</td>';
                    echo '<td>' . $room->getName() . '</td>';
                    echo '<td>' . $room->getPersons() . '</td>';
                    echo '<td>' . $room->getPrice() . '</td>';
                    echo '<td>' . $room->getBalcony() . '</td>';
                    echo '<td>';

                    echo '<a class="btn btn-info" href="view.php?id=' . $room->getNr() . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-primary" href="update.php?id=' . $room->getNr() . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    echo '&nbsp;';
                    echo '<a class="btn btn-danger" href="delete.php?id=' . $room->getNr() . '"><span class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>