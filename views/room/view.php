<?php
$title = "Zimmer anzeigen";
include '../layouts/top.php';
require_once ('../../models/Room.php');

// View single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])){
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    // Load single item per ID
    $room = Room::get($_GET['id']);
}

// Check if item could be found
if($room == null){
    http_response_code(404); // Item not found
    die();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?=$room->getNr()?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?=$room->getNr()?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Zimmernummer</th>
                <td><?=$room->getNr()?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?=$room->getName()?></td>
            </tr>
            <tr>
                <th>Personen</th>
                <td><?=$room->getPersons()?></td>
            </tr>
            <tr>
                <th>Preis</th>
                <td><?=$room->getPrice()?></td>
            </tr>
            <tr>
                <th>Balkon</th>
                <td><?=$room->getBalcony()?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>