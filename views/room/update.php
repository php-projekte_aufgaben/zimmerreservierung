<?php
$title = "Zimmer bearbeiten";
include '../layouts/top.php';
require_once ('../../models/Room.php');

// View single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if(empty($_GET['id'])){
    header('Location: index.php');
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    $room = Room::get($_GET['id']);
}

// Check if item could be found
if ($room == null){
    http_response_code(404); // Item not found
    die();
}

if(!empty($_POST)){
    $room->setName(isset($_POST['name']) ? $_POST['name'] : '');
    $room->setPersons(isset($_POST['size']) ? $_POST['size'] : '');
    $room->setPrice(isset($_POST['price']) ? $_POST['price'] : '');
    $room->setBalcony(isset($_POST['balcony']) ? $_POST['balcony'] : '');

    // Update existing item and redirect to index-page
    if($room->save()){
        header('Location: index.php');
        exit();
    }
}

?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $room->getNr()?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmernummer *</label>
                    <input type="text" class="form-control" name="nr" maxlength="4" value="<?= $room->getNr() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="64" value="<?=$room->getName()?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Personen *</label>
                    <input type="number" class="form-control" name="size" min="1" value="<?=$room->getPersons()?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Preis *</label>
                    <input type="text" class="form-control" name="price" value="<?=$room->getPrice()?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="form-group required ">
                    <label class="control-label">Balkon *</label>
                    <input type="text" class="form-control" name="balcony" checked="<?=$room->getBalcony()?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
