<?php

require_once "models/Database.php";
$db = Database::connect();
$query = file_get_contents("php33.sql");
$stmt = $db->prepare($query);
if($stmt->execute()){
    echo "Success";
} else {
    echo "Failed";
}

Database::disconnect();
header('Location: views/room/index.php');
