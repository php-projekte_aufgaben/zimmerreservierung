<?php

require_once "DatabaseObject.php";

class Guest implements DatabaseObject
{
    private $id;
    private $name;
    private $email;
    private $address;

    private $errors = [];


    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO guest (name, email, address) values (?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->address));
        $lastid = $db->lastInsertId(); //get ID of new database entry
        Database::disconnect();

        return $lastid;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE guest set name = ?, email = ?, address = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->address, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM guest WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to Credentials-object (ORM)
        $guest = $stmt->fetchObject('Guest');
        Database::disconnect();

        return $guest !== false ? $guest : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM guest ORDER BY id ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of Credentials-objects (ORM)
        $guests = $stmt->fetchAll(PDO::FETCH_CLASS, 'Guest');
        Database::disconnect();

        return $guests;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM guest WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    public function validate()
    {
        return $this->validateHelper('Name', 'name', $this->name, 32) &
            $this->validateHelper('E-mail', 'email', $this->email, 28) &
            $this->validateHelper('Adresse', 'address', $this->address, 28);
    }

    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                // known Nr > 0 -> old object -> update
                $this->update();
            } else {
                // undefined Nr -> new object -> create
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


}