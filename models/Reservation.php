<?php

require_once "DatabaseObject.php";

class Reservation implements DatabaseObject
{

    private $id;
    private $startDate;
    private $endDate;
    private $room_id;
    private $mainGuest_id;


    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO reservation (startDate, room_id, mainGuest_id,endDate) values( ?, ?, ?,?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->startDate, $this->room_id, $this->mainGuest_id, $this->endDate));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();

        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE reservation set startDate= ?, room_id = ?, mainGuest_id = ?,endDate = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->startDate, $this->room_id, $this->mainGuest_id, $this->endDate, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM reservation WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to Reservation-object (ORM)
        $item = $stmt->fetchObject('Reservation');
        Database::disconnect();

        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM reservation ORDER BY startDate ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of Credentials-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');
        Database::disconnect();

        return $items;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM reservation WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    /**
     * @return mixed
     */
    public function getMainGuestId()
    {
        return $this->mainGuest_id;
    }

    /**
     * @param mixed $mainGuest_id
     */
    public function setMainGuestId($mainGuest_id)
    {
        $this->mainGuest_id = $mainGuest_id;
    }


}