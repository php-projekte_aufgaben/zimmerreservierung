<?php

require_once "DatabaseObject.php";

class Room implements DatabaseObject
{

    private $nr;
    private $name;
    private $persons;
    private $price;
    private $balcony;

    private $errors = [];


    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO room (name, persons, price, balcony) values( ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->persons, $this->price, $this->balcony));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();

        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE room set name = ?, persons = ?, price = ?, balcony = ? WHERE nr = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->persons, $this->price, $this->balcony, $this->nr));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM room WHERE nr = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        // fetch dataset (row) per ID, convert to Credentials-object (ORM)
        $item = $stmt->fetchObject('Room');
        Database::disconnect();

        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM room ORDER BY nr ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // fetch all datasets (rows), convert to array of Credentials-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Room');
        Database::disconnect();

        return $items;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM room WHERE nr = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    public function validate()
    {
        return $this->validateHelper('Name', 'name', $this->name, 32) &
            $this->validateHelper('Preis', 'price', $this->price, 28);
            $this->validateHelper('Balcony', 'balcony', $this->balcony, 28);
    }

    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }


    public function save()
    {
        if ($this->validate()) {
            if ($this->nr != null && $this->nr > 0) {
                // known Nr > 0 -> old object -> update
                $this->update();
            } else {
                // undefined Nr -> new object -> create
                $this->nr = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @param mixed $nr
     */
    public function setNr($nr)
    {
        $this->nr = $nr;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @param mixed $persons
     */
    public function setPersons($persons)
    {
        $this->persons = $persons;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * @param mixed $balcony
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


}