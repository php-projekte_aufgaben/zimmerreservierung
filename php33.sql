SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `php33`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `guest`
--

CREATE TABLE IF NOT EXISTS `guest` (
    `id` int(11) NOT NULL,
    `name` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `address` varchar(255) NOT NULL
) ENGINE = InnoDB DEFAULT CHAR SET=utf8mb4;

--
-- Daten für Tabelle `gast`
--

INSERT INTO `guest` (`id`, `name`, `email`, `address`) VALUES
(1, 'Michael', 'mibeer@tsn.at', 'Blvd Of-Broken-Dreams 10'),
(2, 'Manuel', 'mforsthuber@tsn.at', 'Luckystreet 13'),
(4, 'Bart', 'bart@simpson.com', 'Evergreen Terrace 742'),
(5, 'Jack', 'jack@sparrow.com', 'Black Pearl 1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reservation`
--

CREATE TABLE `reservation` (
    `id` int(11) NOT NULL,
    `startDate` date NOT NULL,
    `room_id` int(11) NOT NULL,
    `mainGuest_id` int(11) NOT NULL,
    `endDate` date NOT NULL
) ENGINE = InnoDB DEFAULT CHAR SET = utf8mb4;

--
-- Daten für Tabelle `reservation`
--

INSERT INTO `reservation` (`id`, `startDate`, `room_id`, `mainGuest_id`, `endDate`) VALUES
(1, '2022-01-05', 1, 1, '2022-01-19'),
(2, '2022-01-14', 3, 4, '2022-01-21');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `room`
--

CREATE TABLE `room`(
    `nr` int(128) NOT NULL,
    `name` varchar(255) DEFAULT NULL,
    `persons` varchar(255) DEFAULT NULL,
    `price` varchar(255) DEFAULT NULL,
    `balcony` varchar(255) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHAR SET = utf8mb4;

--
-- Daten für Tabelle `room`
--
INSERT INTO `room` (`nr`, `name`, `persons`, `price`, `balcony`) VALUES
(1, 'Tres-Zap123', 4, '€53,72', 'FALSCH'),
(2, 'Pannier', 2, '€53,72', 'FALSCH'),
(3, 'Gembucket', 1, '€81,87', 'WAHR'),
(4, 'Konklab', 5, '€99,81', 'FALSCH'),
(5, 'Zontrax', 10, '€77,96', 'WAHR'),
(6, 'Veribet', 1, '€85,50', 'FALSCH'),
(7, 'Zoolab', 2, '€100', 'WAHR'),
(8, 'Keylex', 3, '€64,09', 'FALSCH'),
(9, 'Regrant', 2, '€43,35', 'FALSCH'),
(15, 'klasjd', 10, '€5', 'WAHR');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `guest`
--
ALTER TABLE `guest`
    ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `reservation`
--
ALTER TABLE `reservation`
    ADD PRIMARY KEY (`id`),
    ADD KEY `mainGuest_id` (`mainGuest_id`),
    ADD KEY `room_id` (`room_id`);

--
-- Indizes für die Tabelle `room`
--
ALTER TABLE `room`
    ADD PRIMARY KEY (`nr`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `guest`
--
ALTER TABLE `guest`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `reservation`
--
ALTER TABLE `reservation`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `room`
--
ALTER TABLE `room`
    MODIFY `nr` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `reservation`
--
ALTER TABLE `reservation`
    ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`mainGuest_id`) REFERENCES `guest` (`id`),
    ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`nr`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;