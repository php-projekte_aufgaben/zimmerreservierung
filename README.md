# Zimmerreservierung

php33 / Zimmerreservierung

# Inhaltsverzeichnis

- [Zimmerreservierung](#zimmerreservierung)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Ausganglsage](#ausganglsage)
  - [Problemstellung](#problemstellung)
  - [Die Situation](#die-situation)
  - [Ziel](#ziel)
  - [Deine Zielgruppe](#deine-zielgruppe)
  - [Das erwartete Produkt](#das-erwartete-produkt)
    - [Funktionen](#funktionen)
  - [Zusatzfunktion](#zusatzfunktion)
  - [Mockup](#mockup)
- [Lösungsansatz](#lösungsansatz)
- [Aufbau der Ordnerstruktur](#aufbau-der-ordnerstruktur)
  - [Algemein](#algemein)
- [Implementierung](#implementierung)
  - [Wichtigsten Codeteile](#wichtigsten-codeteile)
    - [Database.php Class](#databasephp-class)
    - [DatabaseObject.php Interface](#databaseobjectphp-interface)
    - [**CRUD Methoden**](#crud-methoden)
      - [CREATE](#create)
      - [UPDATE](#update)
      - [GET](#get)
      - [GETALL](#getall)
- [ER / UML Diagramm](#er--uml-diagramm)
- [Testprotokoll](#testprotokoll)
  - [Zimmerverwaltung](#zimmerverwaltung)
    - [Zimmer erstellen](#zimmer-erstellen)
    - [Zimmer bearbeiten](#zimmer-bearbeiten)
    - [Zimmer löschen](#zimmer-löschen)
  - [Reservierungen](#reservierungen)
    - [Reservierung erstellen](#reservierung-erstellen)
    - [Reservierung anzeigen](#reservierung-anzeigen)
    - [Reservierung bearbeiten](#reservierung-bearbeiten)
    - [Reservierung löschen](#reservierung-löschen)
  - [Gästerverwaltung](#gästerverwaltung)
    - [Gast erstellen](#gast-erstellen)
    - [Gast anzeigen](#gast-anzeigen)
    - [Gast bearbeiten](#gast-bearbeiten)
    - [Gast löschen](#gast-löschen)
  - [Navbar](#navbar)
    - [Zimmerreservierung bzw. Reservierung](#zimmerreservierung-bzw-reservierung)
    - [Gäste](#gäste)
    - [Zimmer](#zimmer)


# Ausganglsage

## Problemstellung
Du bist Webentwickler bei der Firma WebDesign GmbH in Imst.

## Die Situation

Ein Hotelier aus dem Ötztal möchte eine Webplattform um seine Gäste und Zimmer verwalten zu können. Er hat 10 unterschiedliche Zimmer und eine Vielzahl an Gästen. Dein Kollege aus der Grafikabteilung hat bereits ein HTML-Mockup erstellt. Die Zimmerdaten stehen als Excel-Liste zur Verfügung.

## Ziel 

Dein Chef hat dich beauftragt einen Prototyp für die Verwaltung von Zimmern, Gästen udn Reservierungen zu erstellen. Eine Benutzerauthentifizierung ist für den Prototyp nicht notwendig.

## Deine Zielgruppe

Die Applikation soll von allen Mitarbeitern des Hotels mit einem herkömmlichen Browser verwendet werden können.

## Das erwartete Produkt
### Funktionen

Der Prototyp sollte mindestens folgendes umfassen:

+ ER-Diagramm und UML-Klassendiagramm
  + Gast: Name, E-Mail, Adresse
  + Zimmer: siehe Exceltabelle
  + Reservierung: Zimmer, Datum von-bis, Hauptgast
+ Installation-Skript (install.php) für die Erstellung der Tabellen (inkl. Testinhalte)
+ Geschäfts- und Persistenzlogik innerhalb der Klassen (Objektrelationales Mapping (ORM), Active-Record-Pattern)
  + Create, Read, Update und Delete (CRUD) Operationen
+ Trennung von Darstellung und Logik/Datenhaltung
+ Validierung der Eingabedaten im jeweiligen Modell
+ Server- und Clientseitige Validierung der Eingabedaten
+ Intergration der Anwendung in das vorgegebene Mockup



## Zusatzfunktion

+ Kollisionserkennung bei Zimmerreservierungen
  
<br>

## Mockup

![FertigesMockup](Mockups/Ansicht.png)

<br>

# Lösungsansatz

In der letzen Teamsitzung deines Projektteams wurde gemeinsam folgender Lösungsansatz entwickelt, der von dir im Folgenden umgesetzt werden soll.

1. UML-Klassendiagramm und ER-Diagramm erstellt
   + Tabelle Gast anlegen (Attribute: zumindest ID, Name, E-Mail, Adresse)
   + Tabelle Zimmer anlegen (Attribute: ID, Nr, Name, Personen, Preis, Balkon)
   + Tabelle Reservierung anlegen (Attribute: ID, Gast_ID, Zimmer_ID, Start, Ende)
     + Tabelle Reservierung verwendet die Attribute Gast_ID & ZImmer_ID
     + Klasse Reservierung verwendet die Datenfelder Gast & Zimmer (enthält jeweils ein Objekt)
     + Index/Fremdschlüssel des Gastes auf Tabelle Gast einrichten
     + Index/Fremdschlüssel des Zimmers auf Tabelle Zimmer einrichten
2. Zimmerdaten aus CSV über PHPMyAdmin importieren
    + Anpassung von einigen Werten notwendig (Kommazeichen, Eurozeichen, führende Null, True/False auf 0/1, etc)
3. Installationsskript erstellen
    + Tabellen über PHPMyAdmin als SQL exportieren (inkl. CREATE TABLE)
    + SQL-Befehle im install-Skript mit exec ausführen
    + Link zur index.php einbauen
4. Klassen Gast, Zimmer & Reservierung erstellen
    + Datenferlder (entsprechend den Attributen der zugehörigen Tabelle)
      + Reservierung: Datenfeld Gast anstatt von Gast_ID
      + Reservierung: Datenfeld Zimmer anstatt von Zimmer_ID
    + Konstruktor, Get-/Setmethoden
    + Validierungsmethoden für alle Attribute
    + Implementierung des Interface DatabaseObject
      + Sämtliche Presistenzlogik befindet sich in der Klasse
      + Jeglicher Datenbankzugriff erfolgt mittels Prepared-Statements
    + Hinweis: get- und getAll in Reservierung sollte mittels JOIN auch die Gast- und Zimmer-Daten laden
    + Views ausprogrammieren

<br>

# Aufbau der Ordnerstruktur

**php33-angabe**
+ **css**
+ **fonts**
+ **img**
+ **js**
+ **models**
  + Database.php (Class)
  + DatabaseObject.php (Interface)
  + Guest.php (Class)
  + Reservation.php (Class)
  + Room.php (Class)
+ **views**
  + guest
    + create.php
    + delete.php
    + index.php
    + update.php
    + view.php
  + layouts
    + bottom.php
    + top.php
  + reservation
    + create.php
    + delete.php
    + index.php
    + update.php
    + view.php
  + **room**
    + create.php
    + delete.php
    + index.php
    + update.php
    + view.php
+ index.php
+ installSQL.php
+ php33.sql

<br>

## Algemein

Active Record Pattern = Geschäfts/Buissness/Persistenzlogik in der models Klasse.

ORM = Objekt Relationales Mapping!
Vorteil = Objekte im Code verwedet werden können! ohne SQL statements oder Assoziativen arrays arbeiten zu müssen.


# Implementierung 

## Wichtigsten Codeteile

**models Package** hält die Geschäfts bzw. Business und Persistenzlogik. Dies wird auch als **Active Record Pattern** bezeichnet. 

### Database.php Class

Die Database.php Klasse beinhaltet alle wichtigen Informationen zum aufsetzen der Datenbank.
Vier priavate und statische Variablen die den Datenbanknamen, den Host, den Username und das Passwort beinhalten sowie eine Variable welche die connection auf **null** setzt.

```php
    private static $dbName = 'php33';
    private static $dbHost = 'mysql';
    private static $dbUsername = 'root';
    private static $dbUserPassword = '123';
    private static $conn = null;
```
<br>

Darunter befindet sich die **magische Methode __construct()** welcher lediglich die exit function aufruft und die Meldung "Init function is not allowed" ausgibt. **exit()** beendet das script und die mitgegeben nachricht weißt darauf hin das die **Init Functions** nicht erlaubt sind. Init Functionen sind bestantteile des **Zend Framework** welches ein komponentenorientiertes Webframwork für PHP ist. Klassen und Pakete können unabhängig voneinander und auch in Kombination mit der Lösung anderer Hersteller genutzt werden. Der Construktor wird beim verwenden externer Librarys aufgerufen.

```php
 public function __construct()
    {
        exit('Init function is not allowed');
    }
```
<br>

In diser Function wird die Verbindung überprüft und hergestellt.

```php
    public static function connect()
    {
        // One connection through whole application
        if (null == self::$conn) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

```

### DatabaseObject.php Interface

Dieses Interface stellt die **CRUD** (**CREATE, RETRIEVE, UPDATE & DELETE**) operation zur Verfügung welche in den Klassen **Guest**, **Reservation** und **Room** implementiert werden.

```php

require_once 'Database.php';

interface DatabaseObject
{
    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create();

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update();

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id);

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll();

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id);
}
```

<br>

### **CRUD Methoden**

Die CRUD Methoden in den den KLassen **Guest, Room und Reservation** sind sich sehr ähnlich. Im weitern Verlauf wird die Klasse **Room** zur Erklärung herangezogen.

Die Klasse **Room** implementiert die Methoden aus dem Interface **DatabaseObject**. Diese Klasse beinhaltet fünf private Datenferlder. **nr, name, persons, price, balcony** sowie ein **erorrs array[]**.

#### CREATE

Die erste Methode in dieser Klasse ist die **create()**. An der Klasse **Database** wird die statische Methode **connect();** aufgerufen werlche eine Verbindung zur Datenbank herstellt und anschließend in die Variable **db** gespeichert. Anschließend folgt ein **SQL INSERT INTO** statement wo in die Tabelle **room** folgende daten in die Spalten gespeichert werden **(name, persons, price, balcony)** mit den **Werten/values(?,?,?,?)**. Die Fragezeichen sind Platzhalter für die Benutzereingabe. Dieser **INSERT** Befehl wird in die Variable **sql** gespeichert. Durch **db->prepare(sql)** wird nach dem erfolgreichem Verbinden mit der Datenbank das **INSERT Statment** das in der Variable **sql** gespeichert wurde **prepared** sprich vorbereitet und in die Variable **db** aslo in die **Datanbank** gespeichert. Diese werden wiederum in die Variable **stmt** gespeichert. Das **Array[]** mit allen Parametern wird durch die **execute** Methode welche durch die **stmt** Variabel aufgerufen wird ausgeführt.
Dann wird an der **db** Variable die Methode **lastInsertgId()** aufgerufen welche die zuletzt eingefügte **ID** ausgibt. Anschließend wird dies in die Variable **lastId** gepseichert die schlussendlich **returned** wird.
Nach erfolgreichem ausführen wird die Verbindung getrennt mit dem aufruf der statsichen Methode **disconnect()**. 


```php
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO room (name, persons, price, balcony) values( ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->persons, $this->price, $this->balcony));
        $lastId = $db->lastInsertId();  // get ID of new database-entry
        Database::disconnect();

        return $lastId;
    }
```

#### UPDATE 

Die Update Funktion ist so gut wie Identisch nur ein anderes SQL Statement

```php
 $sql = "UPDATE room set name = ?, persons = ?, price = ?, balcony = ? WHERE nr = ?";
```

#### GET

Die Function get hat als Parameter die Variabel **id**.
Auch dises Funktion ist wie die Update sehr ähnlich zur Create Funktion.
Zusätzliche Aspekte sind ein anderes **SQL** Statement sowie die Mehtode **fetchObject('Room')** welche an der Variable **stmt** aufgerufen wird und in die **item** Variable gespeichert wird. Die **fetchObject()** Methode gibt ein Objekt der Klasse **Raum** zurück da die Klasse **Raum** als Parameter mitgegeben wurde.

Zum Schluss wird geprüft ob das **Object(item)** nicht **false** oder nicht gleich **null** ist. 

```php
$sql = "SELECT * FROM room WHERE nr = ?";
$item = $stmt->fetchObject('Room');
return $item !== false ? $item : null;
```

#### GETALL

Zusätzlich ist in der **GetAll()** Funktion die **fetchAll()** Methode die an der Variable **stmt** aufgerufen wird.
Die fetchAll() Merhode hat als parameter einmal **mode** mit **PDO (PHP Data Object)** welche die Verbindung zwischen dem PHP Code und der Datenbank herstellt, mit **FETCH:CLASS** wird eine neue Instanz der gewünschten Klasse zurückgegeben und als Argument die Klasse **Room**.

```php
 $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Room');
```
<br>

# ER / UML Diagramm


![ER-Diagramm](Diagramme/ER-Diagramm.png)

<br>

![UML-Diagramm](Diagramme/UML-Diagramm.png)

<br>

# Testprotokoll

## Zimmerverwaltung

![Zimmerverwaltung](images/Zimmerverwaltung1.png)

### Zimmer erstellen

![Room-Create](images/room_create.png)

![Zimmer-erstellen](images/Zimmer_erstellen.png)

![Room-Create-View](images/room_create_view.png)

![Room-Create-View-Header1](images/room_create_view_header1.png)

![Room-Create-View-Header2](images/room_create_view_header2.png)

![Zimmer-anzeigen](images/zimmer_anzeigen.png)

![Ziimmerverwaltung2](images/Zimmerverwaltung2.png)

### Zimmer bearbeiten

![Update-Header](images/update_header.png)

![Update-Header2](images/update_header2.png)

![Zimmer-bearbeiten](images/Zimmer_bearbeiten.png)

![Room-Update-Header3](images/room_update_header3.png)

![Room-Update-Header4](images/room_update_header4.png)

![Room-Update-Header5](images/room_update_header5.png)

![Zimmerverwaltung3](images/Zimmerverwaltung3.png)

### Zimmer löschen

![Room-Delete-Header1](images/room_delete_header1.png)

![Room-delete-Header2](images/room_delete_header2.png)

![Zimmer-löschen](images/Zimmer_löschen.png)

![Room-Delete-Header3](images/room_delete_header3.png)

![Room-Delete-Header4](images/room_delete_header4.png)

![Room-Delete-Header5](images/room_delete_header5.png)

![Zimmerverwaltung4](images/Zimmerverwaltung4.png)

## Reservierungen

![Reservierung1](images/Reservierung1.png)

![Reservation-Header1](images/reservation_header1.png)

![Reservation-Header2](images/reservation_header2.png)

### Reservierung erstellen

![Reservierung-Erstellen1](images/Reservierung_erstellen1.png)

![Reservation-Header3](images/reservation_header3.png)

![Reservation-Header4](images/reservation_header4.png)

![Reservation-Header5](images/reservation_header5.png)

![Reservation-Header6](images/reservation_header6.png)

![Reservation-Header7](images/reservation_header7.png)

![Reservation-Header8](images/reservation_header8.png)

![Reservierung2](images/Reservierung2.png)

### Reservierung anzeigen

![Reservierung-Anzeigen](images/Reservierung_anzeigen.png)

![Reservation-Header9](images/reservation_header9.png)

![Reservation-Header10](images/reservation_header10.png)

### Reservierung bearbeiten

![Reservierung-Bearbeiten](images/Reservierung_bearbeiten.png)

![Reservation-Update-Header1](images/reservation_update_header1.png)

![Reservation-Update-Header2](images/reservation_update_header2.png)

![Reservation-Update-Header3](images/reservation_update_header3.png)

![Reservation-Update-Header4](images/reservation_update_header4.png)

![Reservation-Update-Header5](images/reservation_update_header5.png)

![Reservierung3](images/Reservierung3.png)

### Reservierung löschen

![Reservation-Delete-Header1](images/reservation_delete_header1.png)

![Reservation-Delete-Header2](images/reservation_delete_header2.png)

![Reservierung-Löschen](images/Reservierung_löschen.png)

![Reservation-Delete-Header3](images/reservation_delete_header3.png)

![Reservation-Delete-Header4](images/reservation_delete_header4.png)

![Reservation-Delete-Header5](images/reservation_delete_header5.png)

![Reservierung4](images/Reservierung4.png)

## Gästerverwaltung

![Gästerverwaltung1](images/Gästerverwaltung1.png)

### Gast erstellen

![Gast-Erstellen](images/Gast_erstellen.png)

![Guest-Create-Header1](images/guest_create_header1.png)

![Guest-Create-Header2](images/guest_create_header2.png)

![Guest-Create-Header3](images/guest_create_header3.png)

![Guest-Create-Header4](images/guest_create_header4.png)

![Guest-Create-Header5](images/guest_create_header5.png)

![Gästerverwaltung2](images/Gästerverwaltung2.png)

### Gast anzeigen

![Gast-Anzeigen](images/Gast_anzeigen.png)

![Guest-View-Header1](images/guest_view_header1.png)

![Guest-View-Header2](images/guest_view_header2.png)

### Gast bearbeiten

![Gast-Bearbeiten](images/Gast_bearbeiten.png)

![Guest-Update-Header1](images/guest_update_header1.png)

![Guest-Update-Header2](images/guest_update_header2.png)

![Guest-Update-Header3](images/guest_update_header3.png)

![Guest-Update-Header4](images/guest_update_header4.png)

![Guest-Update-Header5](images/guest_update_header5.png)

![Gastverwaltung](images/Gastverwaltung.png)

### Gast löschen

![Gast-Löschen](images/Gast_löschen.png)

![Guest_Delete-Header1](images/guest_delete_header1.png)

![Guest_Delete-Header2](images/guest_delete_header2.png)

![Guest_Delete-Header3](images/guest_delete_header3.png)

![Guest_Delete-Header4](images/guest_delete_header4.png)

![Guest_Delete-Header5](images/guest_delete_header5.png)

![Gästerverwaltung](images/Gästerverwaltung3.png)

## Navbar

![Navbar](images/navbar.png)

### Zimmerreservierung bzw. Reservierung

![Navbar-Header1](images/navbar_header1.png)

![Navbar-Header2](images/navbar_header2.png)

### Gäste

![Navbar-Header3](images/navbar_header3.png)

![Navbar-Header4](images/navbar_header4.png)

### Zimmer

![Navbar-Header5](images/navbar_header5.png)

![Navbar-Header6](images/navbar_header6.png)